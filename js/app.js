var listOfCats = [
    {
        catName: "john",
        img: "img/22252709_010df3379e_z.jpg",
        clickCount: 0,
        nicknames: [
            {name: 'Johnjohn'},
            {name: 'Jonhyn'},
            {name: 'baby'},
            {name: 'lubby'}
        ]
    },
    {
        catName: "ruppet",
        img: "img/434164568_fea0ad4013_z.jpg",
        clickCount: 0,
        nicknames: [{name: 'rupper'}]
    },
    {
        catName: "dorian and lyly",
        img: "img/1413379559_412a540d29_z.jpg",
        clickCount: 0,
        nicknames: [{name: 'dor n li'}]
    }
]

var Cat = function(data){
    this.catName = ko.observable(data.catName);
    this.imgSrc = ko.observable(data.img);
    this.clickCount = ko.observable(data.clickCount);
    this.nicknames = data.nicknames

    this.level = ko.computed(function(){
        if (this.clickCount() < 10){
            return 'filhote';
        } else if (this.clickCount() < 30){
            return 'Jovem'
        } else if (this.clickCount() < 75){
            return 'putuquinho'
        } else if (this.clickCount() < 200){
            return 'putucão'
        } else if (this.clickCount() > 199){
            return 'ancião'
        } 
    }, this);
}

var ViewModel = function(){
    var self = this;

    this.catList = ko.observableArray([]);

    this.setCurrentCat = function(e){
        self.currentCat(e)
    }

    listOfCats.forEach(function(cat){
        self.catList.push(new Cat(cat));
    })

    this.currentCat = ko.observable(this.catList()[0])
    // var self = this
    this.increaseCount = function() {
        this.clickCount(this.clickCount() + 1);
        // self.currentCat().clickCount(self.currentCat().clickCount() + 1)
    };
}

ko.applyBindings(new ViewModel());